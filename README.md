# Transforms-Moving-3D-Object
mGraphics3D.bindTarget(g);
mGraphics3D.clear(mBackground);

mTransform.setIdentity();
mTransform.postTranslate(0.0f, 0.0f, 10.0f);
mGraphics3D.setCamera(mCamera, mTransform)

mGraphics3D.resetLights();
mGraphics3D.addLight(mLight, mTransform);

void set Scale(float sx, float xy; float sz);
void setTranslation(float tx, float ty, float tz);
void preRotate(float angle, float ax, float ay float az);
void postRotate(float angle, float ax, float ay, float az);
void setOrientation(float angle, float ax, float ay, float az);

